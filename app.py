from flask import Flask, request, render_template
import json

app = Flask(__name__)

tasks = [
    {
        "RA": 2200220
        "nome":"Elias de Arimateia Souza Leal"
    }

@app.route('/')
def first():
    return '<h1>AC05 - DevOps</h1>'

@app.route('/health-check')
def hello():
    return '<h1>AC05 - DevOps</h1>'


@app.route('/consultar/<int:RA>', methods=["GET"])
def consultar(RA):
    try:
        for task in tasks:
            if RA == task["RA"]:
                return task
    except:
        return {"error": "Usuário não encontrado"} 
       
@app.route('/usuarios', methods=["GET"])
def verificar():        
    return{"usuarios": tasks}

@app.route('/cadastrar', methods=["POST"])
def cadastrar():
    input_json = request.get_json()
    tasks.append(input_json)
    return {"Resultado":"JSON adicionado com sucesso. Lista de dados atualizada"}

@app.route('/deletar/<int:id>', methods=["DELETE"])
def deletar(id):
    for task in tasks:
        if id == task["id"]:
            tasks.remove(task)
    return "Excluido usuário de Id: " + str(id)
    
@app.route('/atualizar/<int:id>', methods=["PUT"])
def atualizar(id):
    input_json = request.get_json()
    print(input_json)
    for task in tasks:
        if id == task["id"]:
            task["nome"] = input_json["nome"]
            return "Usuário de id:" + str(id) + " foi atualizado!"
        

if __name__ == '__main__':
    app.run(debug=True)
