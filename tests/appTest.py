import unittest
from app import app

class AppTest(unittest.TestCase):
    def setUp(self):
        self.app = app.test_client()

    def test_print_ac05_DevOps(self):
        response = self.app.get('/health-check')
        self.assertEqual("<h1>AC05 - DevOps</h1>", response.get_data(as_text=True)
                         , "Erro de validação da página")

    def test_http_status(self):
        response = self.app.get('/health-check')
        self.assertEqual(200, response.status_code, "Erro de status code")

    def test_http_aluna(self):
        response = self.app.get('/consultar/2200519')
        self.assertIn("Amalia", response.get_data(as_text=True)
                         , "Erro ao encontrar aluna Amalia")  

    def test_http_usuarios(self):
        response = self.app.get('/usuarios')
        self.assertIn("Guilherme", response.get_data(as_text=True)
                         , "Erro ao encontrar aluno Guilherme")  

if __name__ == "__main__":
    unittest.main()
